FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml --batch-mode clean package


FROM cluster-apache-spark:3.0.2

COPY --from=build /home/app/target/Lab2-1.0-SNAPSHOT-jar-with-dependencies.jar /opt/spark/applications/Labs2.jar

ENV JAR_FILEPATH="/opt/spark/applications/Labs2.jar"
ENV CLASS_TO_RUN="Main"
ENV PARAMS=""

ADD run.sh /run.sh
RUN chmod a+x /run.sh

CMD ["/run.sh"]