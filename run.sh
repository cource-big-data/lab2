#!/bin/bash

$SPARK_HOME/bin/spark-submit --master spark://spark-master:7077 \
--driver-memory 1G \
--executor-memory 1G \
$JAR_FILEPATH $PARAMS