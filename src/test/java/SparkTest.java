import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.course.spark.SparkService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SparkTest {
    SparkService sparkService;
    private static String sparkMaster = "local[4]";



    SparkConf sparkConf;

    JavaStreamingContext sc;

    @BeforeEach
    public void setUp()
    {
        sparkConf = new SparkConf()
                .setAppName("Flight")
                .setMaster(sparkMaster);

        sc = new JavaStreamingContext(sparkConf, Durations.seconds(1));
        sparkService = new SparkService(sc);
    }

    @SneakyThrows
    @Test
    public void testCountFlightEquallyAirport()
    {
        // Create the queue through which RDDs can be pushed to a QueueInputDStream
        final Queue<JavaRDD<String>> rddQueue = new LinkedList<>();

        List<String> strings = Lists.newArrayList();
        strings.add("A0001,2020-01-23 15:14:12,2020-01-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Голуэй,Ирландия");
        strings.add("A0001,2020-01-23 15:24:12,2020-01-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Голуэй,Ирландия");
        strings.add("A0001,2020-01-23 16:24:12,2020-01-23 17:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Голуэй,Ирландия");
        strings.add("A0001,2020-02-23 15:24:12,2020-02-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Голуэй,Ирландия");

        rddQueue.add(sc.sparkContext().parallelize(strings));
        var test = sc.queueStream(rddQueue);
        var res = sparkService.countFlightCountry(test);

        sc.start();
        sc.awaitTerminationOrTimeout(2000);

        Map<String, Integer> map2 = new HashMap<>();
        map2.put("2020-01-23 15:00:00,Великобритания,Ирландия", Integer.valueOf(2));
        map2.put("2020-01-23 16:00:00,Великобритания,Ирландия", Integer.valueOf(1));
        map2.put("2020-02-23 15:00:00,Великобритания,Ирландия", Integer.valueOf(1));

        sc.stop();

        assertEquals(res, map2);

    }

    @SneakyThrows
    @Test
    public void testCountFlightEquallyCountry()
    {
        // Create the queue through which RDDs can be pushed to a QueueInputDStream
        final Queue<JavaRDD<String>> rddQueue = new LinkedList<>();

        List<String> strings = new ArrayList<String>();
        strings.add("A0001,2020-01-23 15:14:12,2020-01-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Голуэй,Ирландия");
        strings.add("A0001,2020-01-23 15:17:12,2020-01-23 16:14:12,Аэропорт Глазго Прествик,Великобритания,Аэропорт Голуэй,Ирландия");
        strings.add("A0001,2020-01-23 15:47:12,2020-01-23 16:54:12,Аэропорт Глазго Прествик,Великобритания,Аэропорт Дублин,Ирландия");
        strings.add("A0001,2020-01-23 15:18:12,2020-01-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Дублин,Ирландия");
        strings.add("A0001,2020-01-23 16:18:12,2020-01-23 17:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Дублин,Ирландия");
        strings.add("A0001,2020-02-23 15:18:12,2020-02-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Дублин,Ирландия");

        rddQueue.add(sc.sparkContext().parallelize(strings));
        var test = sc.queueStream(rddQueue);
        var res = sparkService.countFlightCountry(test);

        sc.start();
        sc.awaitTerminationOrTimeout(2000);

        Map<String, Integer> map2 = new HashMap<>();
        map2.put("2020-01-23 15:00:00,Великобритания,Ирландия", Integer.valueOf(4));
        map2.put("2020-01-23 16:00:00,Великобритания,Ирландия", Integer.valueOf(1));
        map2.put("2020-02-23 15:00:00,Великобритания,Ирландия", Integer.valueOf(1));

        sc.stop();

        assertEquals(res, map2);
    }

    @SneakyThrows
    @Test
    public void testCountFlight()
    {
        // Create the queue through which RDDs can be pushed to a QueueInputDStream
        final Queue<JavaRDD<String>> rddQueue = new LinkedList<>();

        List<String> strings = new ArrayList<String>();
        strings.add("A0001,2020-01-23 15:14:12,2020-01-23 16:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Голуэй,Ирландия");
        strings.add("A0001,2020-01-23 15:17:12,2020-01-23 18:14:12,Аэропорт Гибралтар,Великобритания,Аэропорт Задар,Хорватия");
        strings.add("A0001,2020-01-23 15:17:12,2020-01-23 18:14:12,Аэропорт Задар,Хорватия,Аэропорт Гибралтар,Великобритания");
        strings.add("A0001,2020-01-23 15:56:12,2020-01-23 16:58:12,Аэропорт Будапешт Ференц Лист,Венгрия,Аэропорт Берн Бельп,Швейцария");

        rddQueue.add(sc.sparkContext().parallelize(strings));
        var test = sc.queueStream(rddQueue);
        var res = sparkService.countFlightCountry(test);

        sc.start();
        sc.awaitTerminationOrTimeout(2000);

        Map<String, Integer> map2 = new HashMap<>();
        map2.put("2020-01-23 15:00:00,Великобритания,Ирландия", Integer.valueOf(1));
        map2.put("2020-01-23 15:00:00,Великобритания,Хорватия", Integer.valueOf(1));
        map2.put("2020-01-23 15:00:00,Хорватия,Великобритания", Integer.valueOf(1));
        map2.put("2020-01-23 15:00:00,Венгрия,Швейцария", Integer.valueOf(1));

        sc.stop();

        assertEquals(res, map2);
    }
}
