package ru.course.spark;

import lombok.SneakyThrows;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import ru.course.db.CassandraService;
import scala.Tuple2;


import java.util.*;


public class SparkService {

    private JavaStreamingContext sc;
    private CassandraService service;
    /**
     * Метод инициализации необходимых параметров
     */
    public SparkService()
    {

        SparkConf sparkConf = new SparkConf()
                .setAppName("Flight")
                .setMaster("spark://spark-master:7077");
        sc = new JavaStreamingContext(sparkConf, Durations.seconds(1));
    }

    public SparkService(CassandraService cassandraService)
    {
        this();
        service = cassandraService;
    }

    public SparkService(CassandraService cassandraService, JavaStreamingContext sc)
    {
        this(sc);
        service = cassandraService;
    }

    public SparkService(JavaStreamingContext sc)
    {
        this.sc = sc;
    }


    /**
     * считываем данные из kafka
     * возвращаем словарь
     * ключь является связка вида: Город вылета,Город прилета,Дата вылета
     * значение количество таких данных
     * @return
     */
    @SneakyThrows
    public Map<String, Integer> countFlightCountry(JavaDStream<String> lines)
    {

        JavaDStream<String> stringDStream = lines
                .map(x -> {
                    String[] strings = x.split(",");
                    return String.join(",", strings[1].substring(0, 14)+"00:00",strings[4],strings[6]);
                });
        JavaPairDStream<String, Integer> airCounts = stringDStream.mapToPair(s -> new Tuple2<>(s, 1))
                .reduceByKey(Integer::sum);

        Map<String, Integer> res = new HashMap<>();
        airCounts.foreachRDD(
                javaRdd -> {
                    Map<String, Integer> airCountMap = javaRdd.collectAsMap();
                    res.putAll(airCountMap);
                    airCountMap.forEach((x, y) -> System.out.print(x + "," + y + "\n"));
                    if(service != null)
                        service.insertAllFlight(airCountMap);
                }
        );
        return res;
    }
}
