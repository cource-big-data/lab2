package ru.course;

import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import ru.course.db.CassandraService;
import ru.course.spark.SparkService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

public class Main {

    @SneakyThrows
    public static void main(String[] args) {

        SparkConf sparkConf = new SparkConf()
                .setAppName("Flight")
                .setMaster("local");
        JavaStreamingContext sc = new JavaStreamingContext(sparkConf, Durations.seconds(1));

        Collection<String> topics = Arrays.asList("stream-spark");
        JavaInputDStream<ConsumerRecord<String, String>> stream =
                KafkaUtils.createDirectStream(
                        sc,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.Subscribe(topics, loadKafkaConfig())
                );
        JavaDStream<String> lines = stream.map(ConsumerRecord::value);
        SparkService sparkService = new SparkService(new CassandraService(), sc);
        sparkService.countFlightCountry(lines);

        sc.start();
        sc.awaitTermination();
    }

    private static Map<String, Object> loadKafkaConfig() throws IOException {

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("kafka.properties");
        Properties properties = new Properties();
        properties.load(stream);

        return (Map) properties;
    }
}
