package ru.course.db;

import com.datastax.oss.driver.api.core.CqlSession;

import java.util.Map;
import java.util.UUID;

public class CassandraService {

    CqlSession session;

    public CassandraService(){
        session = CqlSession.builder()
                .withKeyspace("java_api")
                .build();
//        session.execute("DROP KEYSPACE IF EXISTS java_api");
//        session.execute("CREATE KEYSPACE java_api WITH replication= {'class': 'SimpleStrategy', 'replication_factor': 1}");
//        session.execute("CREATE TABLE java_api.flight (id UUID PRIMARY KEY,flight_time TEXT, country_from TEXT, country_to TEXT, flight_count BIGINT)");
    }

    /**
     * Запись одного значения в таблицу flight
     */
    private void insertFlight(String flightTime, String countryFrom, String countryTo, long flightCount)
    {
        session.execute("INSERT INTO java_api.flight (id, flight_time, country_from, country_to, flight_count) VALUES (?, ?, ?, ?, ?)",
                UUID.randomUUID(), flightTime, countryFrom, countryTo, flightCount);
    }

    public void close() {
        session.close();
    }

    /**
     * обработка всех значений полученных SparkRDD
     */
    public void insertAllFlight(Map<String, Integer> map)
    {
        for (Map.Entry entry: map.entrySet())
        {
            String[] strings = entry.getKey().toString().split(",");
            insertFlight(strings[0], strings[1], strings[2], (Integer)entry.getValue());
        }
    }

}
